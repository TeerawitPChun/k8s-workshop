// This line must come before importing any instrumented module.
// const tracer = require('dd-trace').init({
//     hostname: 'datadog-agent',
//     port: 8126
// });
  
const express = require('express')
const os = require('os')
const app = express()
const port = 3000

app.get('/', function (req, res) {
	// res.send('<center><h1>สวัสดี สบายดี Hello World! from  ' + String(os.hostname()) + '</h1></center>')
    res.send('<center><h1>' + process.env.ECHO_MSG + String(os.hostname()) + '</h1></center>')
})

app.listen(port, () => console.log(`Hello World! listening on port ${port}!`))